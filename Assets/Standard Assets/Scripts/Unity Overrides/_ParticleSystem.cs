using UnityEngine;
using System.Collections;
using TimeToDie;
using System.Collections.Generic;

[ExecuteInEditMode]
public class _ParticleSystem : Spawnable
{
	public ParticleSystem particleSystem;

	public override void Start ()
	{
#if UNITY_EDITOR
		base.Start ();
		if (!Application.isPlaying)
		{
			if (particleSystem == null)
				particleSystem = GetComponent<ParticleSystem>();
			return;
		}
#endif
	}

	void OnParticleSystemStopped ()
	{
		ObjectPool.instance.Despawn (prefabIndex, gameObject, trs);
	}
}