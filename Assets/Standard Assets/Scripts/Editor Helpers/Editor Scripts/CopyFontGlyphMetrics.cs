#if UNITY_EDITOR
using TMPro;
using Extensions;
using UnityEditor;
using UnityEngine;
using System.Collections;
using UnityEngine.TextCore;
using System.Collections.Generic;

namespace TimeToDie
{
	[ExecuteInEditMode]
	public class CopyFontGlyphMetrics : EditorScript
	{
		public TMP_FontAsset copyFromFontAsset;
		public TMP_FontAsset copyToFontAsset;

		public override void Do ()
		{
			for (int i = 0; i < copyFromFontAsset.glyphTable.Count; i ++)
			{
				Glyph glyph = copyFromFontAsset.glyphTable[i];
				copyToFontAsset.glyphLookupTable[glyph.index].metrics = glyph.metrics;
			}
		}
	}
}
#else
namespace TimeToDie
{
	public class CopyFontGlyphMetrics : EditorScript
	{
	}
}
#endif