﻿using TMPro;
using System;
using UnityEngine;
using System.Collections;

namespace TimeToDie
{
	[Serializable]
	public class TemporaryActiveText : TemporaryActiveGameObject
	{
		public _Text text;
		public float durationPerCharacter;
		
		public override IEnumerator DoRoutine ()
		{
			duration = text.Text.Length * durationPerCharacter;
			yield return base.DoRoutine ();
		}
	}
}