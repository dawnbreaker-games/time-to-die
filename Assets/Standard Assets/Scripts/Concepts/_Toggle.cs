﻿using System;
using Extensions;
using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

namespace TimeToDie
{
	public class _Toggle : UpdateWhileEnabled
	{
		public RectTransform rectTrs;
		public GameObject isOnIndicator;
		public bool isOn;
		public Material mousedOverMaterial;
		public Material notMousedOverMaterial;
		public Renderer renderer;
		public UnityEvent<bool> onValueChanged;
		Shape3D.Face face;

#if UNITY_EDITOR
		void OnValidate ()
		{
			isOnIndicator.SetActive(isOn);
		}
#endif

		void Awake ()
		{
			Vector3[] corners = new Vector3[4];
			rectTrs.GetWorldCorners(corners);
			face = new Shape3D.Face(corners);
		}

		public override void DoUpdate ()
		{
			LineSegment3D leftHandLineSegment = new LineSegment3D(VRCameraRig.instance.leftHand.trs.position, VRCameraRig.instance.leftHand.trs.forward * 99999);
			LineSegment3D rightHandLineSegment = new LineSegment3D(VRCameraRig.instance.rightHand.trs.position, VRCameraRig.instance.rightHand.trs.forward * 99999);
			bool leftHandMousedOver = face.DoIIntersectWithLineSegment(leftHandLineSegment);
			bool rightHandMousedOver = face.DoIIntersectWithLineSegment(rightHandLineSegment);
			bool mousedOver = leftHandMousedOver || rightHandMousedOver;
			Material material;
			if (mousedOver)
			{
				material = new Material(mousedOverMaterial);
				if ((leftHandMousedOver && VRCameraRig.instance.leftHand.triggerInput && !VRCameraRig.instance.leftHand.previousTriggerInput) || (rightHandMousedOver && VRCameraRig.instance.rightHand.triggerInput && !VRCameraRig.instance.rightHand.previousTriggerInput))
				{
					isOn = !isOn;
					isOnIndicator.SetActive(isOn);
					onValueChanged.Invoke(isOn);
				}
			}
			else
				material = new Material(notMousedOverMaterial);
			renderer.sharedMaterial = material;
		}
	}
}