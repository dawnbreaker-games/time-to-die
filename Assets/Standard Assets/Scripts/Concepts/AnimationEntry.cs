using System;
using UnityEngine;

namespace TimeToDie
{
	[Serializable]
	public struct AnimationEntry
	{
		public string animatorStateName;
		public int layer;
		public Animator animator;

		public void Play (float normalizedTime = float.NegativeInfinity)
		{
			animator.Play(animatorStateName, layer, normalizedTime);
		}

		public bool IsPlaying ()
		{
			return animator.GetCurrentAnimatorStateInfo(layer).IsName(animatorStateName);
		}
	}
}