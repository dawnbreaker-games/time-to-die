﻿using TMPro;
using System;
using Extensions;
using UnityEngine;
using System.Collections;
using UnityEngine.TextCore;
using System.Collections.Generic;

namespace TimeToDie
{
	[ExecuteInEditMode]
	public class _Text : MonoBehaviour
	{
		public RectTransform rectTrs;
		public Transform scalerTrs;
		public Transform textCharactersParent;
		public Transform backgroundTrs;
		[Multiline]
		[SerializeField]
		string text;
		public string Text
		{
			get
			{
				return text;
			}
			set
			{
				if (text != value)
				{
					text = value;
					UpdateText ();
				}
			}
		}
		public TextCharacter textCharacterPrefab;
		public TMP_FontAsset fontAsset;
		public TextAlignmentOptions alignmentOptions;
		[HideInInspector]
		public float minGlyphWidth;
		[HideInInspector]
		public float maxGlyphWidth;
		[HideInInspector]
		public float minGlyphHeight;
		[HideInInspector]
		public float maxGlyphHeight;
		public bool overrideSorting;
		public string sortingLayerName;
		public int sortingOrder;
#if UNITY_EDITOR
		public bool updateText;
		public Color debugBoxColor;

		void OnValidate ()
		{
			if (updateText)
			{
				updateText = false;
				minGlyphWidth = Mathf.Infinity;
				maxGlyphWidth = -Mathf.Infinity;
				minGlyphHeight = Mathf.Infinity;
				maxGlyphHeight = -Mathf.Infinity;
				for (int i = 0; i < fontAsset.glyphTable.Count; i ++)
				{
					Glyph glyph = fontAsset.glyphTable[i];
					GlyphMetrics glyphMetrics = glyph.metrics;
					minGlyphWidth = Mathf.Min(glyphMetrics.horizontalBearingX, minGlyphWidth);
					maxGlyphWidth = Mathf.Max(glyphMetrics.width * glyph.scale + glyphMetrics.horizontalBearingX + glyphMetrics.horizontalAdvance, maxGlyphWidth);
					minGlyphHeight = Mathf.Min(glyphMetrics.horizontalBearingY, minGlyphHeight);
					maxGlyphHeight = Mathf.Max(glyphMetrics.height * glyph.scale + glyphMetrics.horizontalBearingY, maxGlyphHeight);
				}
				UpdateText ();
			}
		}

		void OnDrawGizmos ()
		{
			Vector3[] corners = new Vector3[4];
			rectTrs.GetLocalCorners(corners);
			Vector3 boundsMin = rectTrs.TransformPoint(corners[0]);
			Vector3 boundsMax = rectTrs.TransformPoint(corners[2]);
			Bounds bounds = new Bounds((boundsMax + boundsMin) / 2, Quaternion.Inverse(rectTrs.rotation) * (boundsMax - boundsMin));
			DebugExtensions.DrawBounds (bounds, debugBoxColor, 0, rectTrs.rotation);
		}
#endif

		public virtual void OnEnable ()
		{
#if UNITY_EDITOR
			if (Application.isPlaying)
#endif
			if (gameObject.activeInHierarchy)
				UpdateText ();
		}

		public void UpdateText ()
		{
			for (int i = 0; i < textCharactersParent.childCount; i ++)
			{
				Transform child = textCharactersParent.GetChild(i);
#if UNITY_EDITOR
				if (!Application.isPlaying)
					GameManager.DestroyOnNextEditorUpdate (child.gameObject);
				else
				{
					TextCharacter textCharacter2 = child.GetComponent<TextCharacter>();
					ObjectPool.Instance.Despawn (textCharacter2.prefabIndex, textCharacter2.gameObject, textCharacter2.trs);
					i --;
				}
#else
				TextCharacter textCharacter2 = child.GetComponent<TextCharacter>();
				ObjectPool.Instance.Despawn (textCharacter2.prefabIndex, textCharacter2.gameObject, textCharacter2.trs);
				i --;
#endif
			}
			if (string.IsNullOrEmpty(text))
			{
				gameObject.SetActive(false);
				return;
			}
			scalerTrs.localScale = Vector3.one;
			textCharactersParent.localScale = Vector3.one;
			textCharactersParent.localPosition = Vector3.zero;
			Vector2 drawingPosition = Vector2.zero;
			Vector3[] corners = new Vector3[4];
			rectTrs.GetLocalCorners(corners);
			Vector3 boundsMin = corners[0];
			Vector3 boundsMax = corners[2];
			Bounds bounds = new Bounds((boundsMax + boundsMin) / 2, boundsMax - boundsMin);
			string[] lines = text.Split("\n");
			int maxLineLength = 0;
			for (int i = 0; i < lines.Length; i ++)
			{
				string line = lines[i];
				maxLineLength = Mathf.Max(line.Length, maxLineLength);
			}
			float maxGlyphSizeX = maxGlyphWidth - minGlyphWidth;
			float targetMaxLineWidth = maxGlyphSizeX * text.Length;
			float boundsAspectRatio = bounds.size.x / bounds.size.y;
			float aspectRatiosDistance = Mathf.Infinity;
			float lineHeight = maxGlyphHeight - minGlyphHeight;
			int _lineCount = lines.Length;
			_lineCount += (int) ((float) maxLineLength * maxGlyphWidth / targetMaxLineWidth);
			float _textWidth = targetMaxLineWidth;
			float _textHeight = _lineCount * lineHeight;
			bool sizesAreBothThickerInSameAxis = (_textWidth >= _textHeight) == (bounds.size.x >= bounds.size.y);
			while (targetMaxLineWidth > maxGlyphSizeX)
			{
				int lineCount = lines.Length;
				lineCount += (int) ((float) maxLineLength * maxGlyphWidth / targetMaxLineWidth);
				float textWidth = targetMaxLineWidth;
				float textHeight = lineCount * lineHeight;
				bool newSizesAreBothThickerInSameAxis = (textWidth >= textHeight) == (bounds.size.x >= bounds.size.y);
				if (sizesAreBothThickerInSameAxis != newSizesAreBothThickerInSameAxis)
					break;
				targetMaxLineWidth -= maxGlyphSizeX;
			}
			targetMaxLineWidth += maxGlyphSizeX;
			Vector2 min = VectorExtensions.INFINITE2;
			Vector2 max = -VectorExtensions.INFINITE2;
			bool overTargetMaxLineWidth = false;
			for (int i = 0; i < text.Length; i ++)
			{
				char c = text[i];
				if (c == '\n')
				{
					drawingPosition.x = 0;
					drawingPosition.y -= lineHeight;
					overTargetMaxLineWidth = false;
				}
				else
				{
					TMP_Character character;
					if (fontAsset.characterLookupTable.TryGetValue(c, out character))
					{
						Glyph glyph = character.glyph;
						GlyphRect glyphRect = glyph.glyphRect;
						GlyphMetrics glyphMetrics = glyph.metrics;
						if (!overTargetMaxLineWidth && drawingPosition.x != 0 && drawingPosition.x + glyphMetrics.horizontalBearingX + glyphMetrics.width * glyph.scale > targetMaxLineWidth)
							overTargetMaxLineWidth = true;
						if (overTargetMaxLineWidth && c == ' ')
						{
							drawingPosition.x = 0;
							drawingPosition.y -= lineHeight;
							overTargetMaxLineWidth = false;
						}
						else
						{
							TextCharacter textCharacter = ObjectPool.Instance.SpawnComponent<TextCharacter>(textCharacterPrefab.prefabIndex, rotation:rectTrs.rotation, parent:textCharactersParent);
							Material material = new Material(textCharacter.renderer.sharedMaterial);
							material.SetVector("_glyphPositionInAtlas", new Vector2(glyphRect.x, glyphRect.y));
							material.SetVector("_glyphSizeInAtlas", new Vector2(glyphRect.width, glyphRect.height));
							textCharacter.renderer.sharedMaterial = material;
							if (overrideSorting)
							{
								textCharacter.renderer.sortingLayerName = sortingLayerName;
								textCharacter.renderer.sortingOrder = sortingOrder;
							}
							textCharacter.trs.localScale = new Vector3(glyphMetrics.width, glyphMetrics.height, textCharacter.trs.localScale.z) * glyph.scale;
							Vector2 glyphOffset = new Vector2(glyphMetrics.horizontalBearingX, glyphMetrics.horizontalBearingY);
							Vector2 textCharacterMin = drawingPosition + glyphOffset;
							textCharacter.trs.localPosition = textCharacterMin + (Vector2) textCharacter.trs.localScale / 2;
							min = Vector2.Min(textCharacterMin, min);
							Vector2 textCharacterMax = textCharacterMin + (Vector2) textCharacter.trs.localScale;
							max = Vector2.Max(textCharacterMax, max);
							drawingPosition.x += textCharacter.trs.localScale.x + glyphMetrics.horizontalAdvance;
						}
					}
				}
			}
			if (min.x == Mathf.Infinity)
			{
				gameObject.SetActive(false);
				return;
			}
			Vector2 size = max - min;
			gameObject.SetActive(true);
			Vector2 newLocalScale = bounds.size.Divide(size);
			float minLocalScaleComponent = Mathf.Min(newLocalScale.x, newLocalScale.y);
			scalerTrs.localScale = new Vector3(minLocalScaleComponent, minLocalScaleComponent, 1);
			string alignmentOptionsString = alignmentOptions.ToString();
			bool alignmentOptionsContainsLeft = alignmentOptionsString.Contains("Left");
			bool alignmentOptionsContainsRight = alignmentOptionsString.Contains("Right");
			bool alignmentOptionsContainsDown = alignmentOptionsString.Contains("Bottom");
			bool alignmentOptionsContainsUp = alignmentOptionsString.Contains("Top");
			if (alignmentOptionsContainsLeft)
				textCharactersParent.localPosition = textCharactersParent.localPosition.SetX(bounds.min.x / minLocalScaleComponent);
			else if (alignmentOptionsContainsRight)
				textCharactersParent.localPosition = textCharactersParent.localPosition.SetX(bounds.max.x / minLocalScaleComponent - size.x);
			else
				textCharactersParent.localPosition = textCharactersParent.localPosition.SetX(bounds.center.x / minLocalScaleComponent - size.x / 2);
			if (alignmentOptionsContainsDown)
				textCharactersParent.localPosition = textCharactersParent.localPosition.SetY(bounds.min.y / minLocalScaleComponent);
			else if (alignmentOptionsContainsUp)
				textCharactersParent.localPosition = textCharactersParent.localPosition.SetY(bounds.max.y / minLocalScaleComponent - size.y);
			else
				textCharactersParent.localPosition = textCharactersParent.localPosition.SetY(bounds.center.y / minLocalScaleComponent - size.y / 2);
			backgroundTrs.localPosition = (Vector2) textCharactersParent.localPosition + size / 2;
			textCharactersParent.localPosition = textCharactersParent.localPosition - (Vector3) min;
			backgroundTrs.localScale = size.SetZ(backgroundTrs.localScale.z);
		}
	}
}