﻿using System;
using UnityEngine;
using System.Collections;
using TimeToDie;

[Serializable]
public class TemporaryActiveGameObject
{
	public GameObject go;
	public float duration;
	public bool realtime;

	public void Do ()
	{
		GameManager.instance.StopCoroutine(DoRoutine ());
		GameManager.instance.StartCoroutine(DoRoutine ());
	}
	
	public virtual IEnumerator DoRoutine ()
	{
		go.SetActive(true);
		if (realtime)
			yield return new WaitForSecondsRealtime(duration);
		else
			yield return new WaitForSeconds(duration);
		go.SetActive(false);
	}
}