using System;
using UnityEngine;
using System.Collections.Generic;

[Serializable]
public class BidirectionalMoveableAutoResizableList<T> : List<T>
{
	public bool autoResizeWhenGetAndSetElements;
	public IntRange indexRange = new IntRange();
	public List<T> values = new List<T>();
	public new int Count
	{
		get
		{
			return values.Count;
		}
	}
	public new T this[int index]
	{
		get
		{
			if (autoResizeWhenGetAndSetElements)
			{
				if (index - indexRange.min >= values.Count)
				{
					indexRange = new IntRange(Mathf.Min(indexRange.min, index), Mathf.Max(indexRange.max, index));
					Add (default(T), true);
				}
				else if (index - indexRange.min < 0)
				{
					indexRange = new IntRange(Mathf.Min(indexRange.min, index), Mathf.Max(indexRange.max, index));
					Add (default(T), false);
				}
			}
			return values[index - indexRange.min];
		}
		set
		{
			if (autoResizeWhenGetAndSetElements)
			{
				if (index - indexRange.min >= values.Count)
				{
					indexRange = new IntRange(Mathf.Min(indexRange.min, index), Mathf.Max(indexRange.max, index));
					Add (default(T), true);
				}
				else if (index - indexRange.min < 0)
				{
					indexRange = new IntRange(Mathf.Min(indexRange.min, index), Mathf.Max(indexRange.max, index));
					Add (default(T), false);
				}
			}
			values[index - indexRange.min] = value;
		}
	}

	public BidirectionalMoveableAutoResizableList () : base ()
	{
		autoResizeWhenGetAndSetElements = true;
	}

	public BidirectionalMoveableAutoResizableList (IntRange indexRange, bool autoResizeWhenGetAndSetElements = true)
	{
		this.indexRange = indexRange;
		this.autoResizeWhenGetAndSetElements = autoResizeWhenGetAndSetElements;
		for (int i = indexRange.min; i <= indexRange.max; i ++)
			values.Add(default(T));
	}

	public void Insert (int index, T element, bool toEnd)
	{
		values.Insert(index - indexRange.min, element);
		if (values.Count > 1)
		{
			if (toEnd)
				indexRange.max ++;
			else
				indexRange.min --;
		}
	}

	public bool Remove (T element, bool toEnd)
	{
		bool output = values.Remove(element);
		if (output)
		{
			if (toEnd)
				indexRange.max --;
			else
				indexRange.min ++;
		}
		return output;
	}

	public void RemoveAt (int index, bool toEnd)
	{
		values.RemoveAt(index - indexRange.min);
		if (toEnd)
			indexRange.max --;
		else
			indexRange.min ++;
	}

	public void Add (T element, bool toEnd)
	{
		if (toEnd)
		{
			values.Add(element);
			if (values.Count > 1)
				indexRange.max ++;
		}
		else
		{
			values.Insert(0, element);
			if (values.Count > 1)
				indexRange.min --;
		}
	}
}