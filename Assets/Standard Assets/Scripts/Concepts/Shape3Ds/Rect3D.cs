using System;
using Extensions;
using UnityEngine;

[Serializable]
public class Rect3D : Shape3D
{
	public Vector3 center;
	public Vector3 size;
	public Vector3 rotation;

	public Rect3D (Vector3 center, Vector3 size, Vector3 rotation)
	{
		this.center = center;
		this.size = size;
		this.rotation = rotation;
	}

	public Vector3 GetRandomPoint ()
	{
		Bounds bounds = new Bounds(center, size);
		return Quaternion.Euler(rotation) * (bounds.RandomPoint() - center) + center;
	}
}