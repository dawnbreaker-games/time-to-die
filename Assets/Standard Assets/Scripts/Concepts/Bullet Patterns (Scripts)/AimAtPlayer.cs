﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TimeToDie
{
	[CreateAssetMenu]
	public class AimAtPlayer : BulletPattern
	{
		public override Vector3 GetShootDirection (Transform spawner)
		{
			return Player.instance.trs.position - spawner.position;
		}
	}
}