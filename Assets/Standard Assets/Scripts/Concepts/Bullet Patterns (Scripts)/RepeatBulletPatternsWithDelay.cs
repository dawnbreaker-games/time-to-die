﻿﻿using System;
using UnityEngine;
using System.Collections;

namespace TimeToDie
{
	[CreateAssetMenu]
	public class RepeatBulletPatternsWithDelay : BulletPattern
	{
		// [MakeConfigurable]
		public int repeatCount;
		public BulletPatternEntry[] BulletPatternEntries;

		public override void Init (Transform spawner)
		{
			for (int i = 0; i < BulletPatternEntries.Length; i ++)
			{
				BulletPatternEntry BulletPatternEntry = BulletPatternEntries[i];
				BulletPatternEntry.BulletPattern.Init (spawner);
			}
		}

		public override Bullet[] Shoot (Transform spawner, Bullet bulletPrefab)
		{
			GameManager.instance.StartCoroutine(ShootRoutine (spawner, bulletPrefab));
			return new Bullet[0];
		}
		
		public override Bullet[] Shoot (Vector3 spawnPosition, Vector3 direction, Bullet bulletPrefab)
		{
			GameManager.instance.StartCoroutine(ShootRoutine (spawnPosition, direction, bulletPrefab));
			return new Bullet[0];
		}

		public virtual IEnumerator ShootRoutine (Transform spawner, Bullet bulletPrefab)
		{
			for (int i = 0; i < repeatCount; i ++)
			{
				for (int i2 = 0; i2 < BulletPatternEntries.Length; i2 ++)
				{
					BulletPatternEntry BulletPatternEntry = BulletPatternEntries[i2];
					base.Shoot (spawner, bulletPrefab);
					yield return new WaitForSeconds(BulletPatternEntry.delayNextBulletPattern);
				}
			}
		}

		public virtual IEnumerator ShootRoutine (Vector3 spawnPosition, Vector3 direction, Bullet bulletPrefab)
		{
			for (int i = 0; i < repeatCount; i ++)
			{
				for (int i2 = 0; i2 < BulletPatternEntries.Length; i2 ++)
				{
					BulletPatternEntry BulletPatternEntry = BulletPatternEntries[i2];
					base.Shoot (spawnPosition, direction, bulletPrefab);
					yield return new WaitForSeconds(BulletPatternEntry.delayNextBulletPattern);
				}
			}
		}

		[Serializable]
		public class BulletPatternEntry
		{
			public BulletPattern BulletPattern;
			public float delayNextBulletPattern;
		}
	}
}
