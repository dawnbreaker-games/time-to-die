﻿using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace TimeToDie
{
	[CreateAssetMenu]
	public class ShootAtPlayerWithOffsetThenBurstAndShootBulletPatternFromBulletWhenPastDistanceToPlayer : AimAtPlayer
	{
		public Vector3 shootOffset;
		public Bullet burstBulletPrefab;
		public BulletPattern bulletPattern;
		
		public override Bullet[] Shoot (Transform spawner, Bullet bulletPrefab)
		{
			spawner.forward = Player.instance.trs.position - spawner.position;
			spawner.Rotate(shootOffset);
			Vector3 spawnPosition = spawner.position;
			Bullet bullet = ObjectPool.instance.SpawnComponent<Bullet>(bulletPrefab.prefabIndex, spawnPosition, spawner.rotation);
			bullet.StartCoroutine(BurstRoutine (spawnPosition, bullet));
			return new Bullet[1] { bullet };
		}

		IEnumerator BurstRoutine (Vector3 initPosition, Bullet bullet)
		{
			while (true)
			{
				if ((bullet.trs.position - initPosition).sqrMagnitude >= (Player.instance.trs.position - bullet.trs.position).sqrMagnitude)
				{
					ObjectPool.instance.Despawn (bullet.prefabIndex, bullet.gameObject, bullet.trs);
					bulletPattern.Shoot (bullet.trs, burstBulletPrefab);
					yield break;
				}
				yield return new WaitForEndOfFrame();
			}
		}
	}
}