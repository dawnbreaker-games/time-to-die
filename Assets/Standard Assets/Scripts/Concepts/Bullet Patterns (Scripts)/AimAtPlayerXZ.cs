using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;

namespace TimeToDie
{
	[CreateAssetMenu]
	public class AimAtPlayerXZ : BulletPattern
	{
		public override Vector3 GetShootDirection (Transform spawner)
		{
			return (Player.instance.trs.position - spawner.position).GetXZ();
		}
	}
}