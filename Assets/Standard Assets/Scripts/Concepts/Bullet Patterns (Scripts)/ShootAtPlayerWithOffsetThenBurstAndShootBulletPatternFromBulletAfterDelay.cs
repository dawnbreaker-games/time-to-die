﻿using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace TimeToDie
{
	[CreateAssetMenu]
	public class ShootAtPlayerWithOffsetThenBurstAndShootBulletPatternFromBulletAfterDelay : AimAtPlayer
	{
		public Vector3 shootOffset;
		public float burstDelay;
		public Bullet burstBulletPrefab;
		public BulletPattern bulletPattern;
		public float burstPositionOffset;
		
		public override Bullet[] Shoot (Transform spawner, Bullet bulletPrefab)
		{
			Vector3 direction = Player.instance.trs.position - spawner.position;
			direction = direction.Rotate(Quaternion.Euler(shootOffset));
			Bullet bullet = ObjectPool.instance.SpawnComponent<Bullet>(bulletPrefab.prefabIndex, spawner.position, Quaternion.LookRotation(direction));
			bullet.StartCoroutine(BurstRoutine (bullet));
			return new Bullet[1] { bullet };
		}

		IEnumerator BurstRoutine (Bullet bullet)
		{
			yield return new WaitForSeconds(burstDelay);
			ObjectPool.instance.Despawn (bullet.prefabIndex, bullet.gameObject, bullet.trs);
			bulletPattern.Shoot (bullet.trs, burstBulletPrefab);
			yield break;
		}
	}
}