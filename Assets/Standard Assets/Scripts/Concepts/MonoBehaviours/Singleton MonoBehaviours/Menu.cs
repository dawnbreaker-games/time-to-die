using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace TimeToDie
{
	public class Menu : SingletonUpdateWhileEnabled<Menu>
	{
		public CustomString welcomeTextString;
		Color previousNotificationTextColor;
		string previousNotificationTextString;
		Vector3 playerEulerAnglesOnOpen;
		public static int closedMenuOnFrame;

		public override void Awake ()
		{
			base.Awake ();
			gameObject.SetActive(false);
		}

		public virtual void Open ()
		{
			playerEulerAnglesOnOpen = Player.instance.trs.eulerAngles;
			GameManager.paused = true;
			for (int i = 0; i < Enemy.instances.Count; i ++)
			{
				Enemy enemy = Enemy.instances[i];
				enemy.animator.enabled = false;
			}
			VRCameraRig.instance.camera.cullingMask = LayerMask.GetMask("UI");
			gameObject.SetActive(true);
			previousNotificationTextString = GameManager.instance.notificationText.Text;
			if (SettingsMenu.ShowTutorials)
				GameManager.instance.notificationText.Text = welcomeTextString.Value;
		}

		public virtual void Close ()
		{
			Player.instance.trs.eulerAngles = playerEulerAnglesOnOpen;
			GameManager.paused = false;
			for (int i = 0; i < Enemy.instances.Count; i ++)
			{
				Enemy enemy = Enemy.instances[i];
				enemy.animator.enabled = true;
			}
			gameObject.SetActive(false);
			VRCameraRig.instance.camera.cullingMask = VRCameraRig.instance.whatIsVisible;
			GameManager.instance.notificationText.Text = previousNotificationTextString;
			closedMenuOnFrame = Time.frameCount;
		}
	}
}