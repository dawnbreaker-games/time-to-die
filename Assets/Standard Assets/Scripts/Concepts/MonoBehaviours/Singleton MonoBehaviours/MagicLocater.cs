using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace TimeToDie
{
	public class MagicLocater : SingletonMonoBehaviour<MagicLocater>
	{
		public Transform trs;
		public float range;
		public SerializableDictionary<InputManager.InputDevice, Vector3> localPositionsForInputDevicesDict = new SerializableDictionary<InputManager.InputDevice, Vector3>();

		public override void Awake ()
		{
			base.Awake ();
			localPositionsForInputDevicesDict.Init ();
			trs.localPosition = localPositionsForInputDevicesDict[InputManager.instance.inputDevice];
		}
	}
}