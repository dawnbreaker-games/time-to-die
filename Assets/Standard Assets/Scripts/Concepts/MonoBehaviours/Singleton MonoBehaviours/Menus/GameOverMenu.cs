using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace TimeToDie
{
	public class GameOverMenu : Menu
	{
		public _Text titleText;
		public _Text totalGoldText;
		public _Text hardestFightWonDifficultyText;
		public _Text fightsWonText;
		public _Text bestTotalGoldText;
		public _Text bestHardestFightWonDifficultyText;
		public _Text bestFightsWonText;
		public _Text restartInfoText;
		public CustomString restartInfoString;
		public new static GameOverMenu instance;
		public new static GameOverMenu Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<GameOverMenu>(true);
				return instance;
			}
		}
		public static uint totalGold;
		public static uint hardestFightWonDifficulty;
		public static uint fightsWon;
		public static uint BestTotalGold
		{
			get
			{
				return (uint) PlayerPrefs.GetInt("Best total gold", 0);
			}
			set
			{
				PlayerPrefs.SetInt("Best total gold", (int) value);
			}
		}
		public static uint BestHardestFightWonDifficulty
		{
			get
			{
				return (uint) PlayerPrefs.GetInt("Best hardest fight won", 0);
			}
			set
			{
				PlayerPrefs.SetInt("Best hardest fight won", (int) value);
			}
		}
		public static uint BestFightsWon
		{
			get
			{
				return (uint) PlayerPrefs.GetInt("Best fights won", 0);
			}
			set
			{
				PlayerPrefs.SetInt("Best fights won", (int) value);
			}
		}
		public static uint BestRoomsExplored
		{
			get
			{
				return (uint) PlayerPrefs.GetInt("Best rooms explored", 0);
			}
			set
			{
				PlayerPrefs.SetInt("Best rooms explored", (int) value);
			}
		}

		public override void Awake ()
		{
			base.Awake ();
			totalGold = 0;
			hardestFightWonDifficulty = 0;
			fightsWon = 0;
		}

		public override void Open ()
		{
			base.Open ();
			StartCoroutine(OpenRoutine ());
		}

		IEnumerator OpenRoutine ()
		{
			yield return new WaitForEndOfFrame();
			titleText.UpdateText ();
			totalGoldText.Text += totalGold;
			hardestFightWonDifficultyText.Text += hardestFightWonDifficulty;
			fightsWonText.Text += fightsWon;
			if (totalGold > BestTotalGold)
				BestTotalGold = totalGold;
			bestTotalGoldText.Text += BestTotalGold;
			if (hardestFightWonDifficulty > BestHardestFightWonDifficulty)
				BestHardestFightWonDifficulty = hardestFightWonDifficulty;
			bestHardestFightWonDifficultyText.Text += BestHardestFightWonDifficulty;
			if (fightsWon > BestFightsWon)
				BestFightsWon = fightsWon;
			bestFightsWonText.Text += BestFightsWon;
			restartInfoText.Text = restartInfoString.Value;
		}

		public override void DoUpdate ()
		{
			if (VRCameraRig.instance.leftHand.thumbstickClickedInput || VRCameraRig.instance.rightHand.thumbstickClickedInput)
			{
				Close ();
				_SceneManager.instance.RestartSceneWithoutTransition ();
			}
		}
	}
}