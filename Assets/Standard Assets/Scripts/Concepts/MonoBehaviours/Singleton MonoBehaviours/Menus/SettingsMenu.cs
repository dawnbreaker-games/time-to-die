using System;
using Extensions;
using UnityEngine;
using System.Collections;

namespace TimeToDie
{
	public class SettingsMenu : Menu
	{
		public new static SettingsMenu instance;
		public new static SettingsMenu Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<SettingsMenu>(true);
				return instance;
			}
		}
		public static bool ShowTutorials
		{
			get
			{
				return PlayerPrefsExtensions.GetBool("Show tutorials", true);
			}
			set
			{
				PlayerPrefsExtensions.SetBool("Show tutorials", value);
			}
		}
		public static bool Mute
		{
			get
			{
				return PlayerPrefsExtensions.GetBool("Mute", false);
			}
			set
			{
				PlayerPrefsExtensions.SetBool("Mute", value);
			}
		}
	}
}