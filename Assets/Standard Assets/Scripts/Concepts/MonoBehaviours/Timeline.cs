using Extensions;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace TimeToDie
{
	public class Timeline : UpdateWhileEnabled
	{
		[HideInInspector]
		public float currentTime;
		[HideInInspector]
		public float timeOfLastPoint;
		public List<Point> points = new List<Point>();
		public Image backgroundImage;
		public Transform currentTimeIndicatorTrs;

		public void InsertPointForPlayerAtTime (float previousTime, float time, Player player)
		{
			int pointIndex = GetPointIndexAtTime(time);
			if (pointIndex == points.Count)
			{
				points.Add(new Point(player));
				timeOfLastPoint = time;
				return;
			}
			if (time < previousTime)
			{
				int previousPointIndex = GetPointIndexAtTime(previousTime);
				for (int i = previousPointIndex; i < pointIndex; i ++)
					points.RemoveAt(previousPointIndex);
				pointIndex += previousPointIndex - pointIndex;
			}
			points.Insert(pointIndex, new Point(player));
			timeOfLastPoint = Mathf.Max(time, timeOfLastPoint);
		}

		public void ApplyToPlayerAtTime (float time, Player player)
		{
			Point point = points[GetPointIndexAtTime(time)];
			point.ApplyToPlayer (player);
		}

		int GetPointIndexAtTime (float time)
		{
			int pointCount = points.Count;
			if (time > timeOfLastPoint)
				return pointCount;
			int pointIndex = (int) ((float) pointCount / timeOfLastPoint);
			pointIndex = Mathf.Clamp(pointIndex, 0, pointCount - 1);
			Point point = points[pointIndex];
			int changePointIndex = 1;
			if (point.time > time)
				changePointIndex = -1;
			for (; pointIndex < pointCount; pointIndex += changePointIndex)
			{
				point = points[pointIndex];
				if (Mathf.Sign(point.time - time) == changePointIndex)
					break;
			}
			if (changePointIndex == 1)
				pointIndex --;
			// bool isPointIndexTooSmall;
			// int previousChangeInPointIndex = 0;
			// Point point = new Point();
			// for (; pointIndex < pointCount; pointIndex ++)
			// {
			// 	pointIndex = Mathf.Clamp(pointIndex, 0, pointCount - 1);
			// 	point = points[pointIndex];
			// 	isPointIndexTooSmall = point.time < time;
			// 	if (isPointIndexTooSmall)
			// 	{
			// 		int changeInPointIndex = Mathf.Clamp((int) ((point.time - time) / pointCount), 2, pointCount);
			// 		pointIndex -= changeInPointIndex;
			// 		if (changeInPointIndex == 2 && previousChangeInPointIndex == 1)
			// 			break;
			// 		previousChangeInPointIndex = changeInPointIndex;
			// 	}
			// 	else
			// 		previousChangeInPointIndex = 1;
			// }
			return pointIndex;
		}

		public override void DoUpdate ()
		{
			currentTimeIndicatorTrs.localScale = currentTimeIndicatorTrs.localScale.SetX(timeOfLastPoint / currentTime);
			currentTimeIndicatorTrs.parent.localScale = currentTimeIndicatorTrs.parent.localScale.SetX(currentTime / timeOfLastPoint);
		}

		public struct Point
		{
			public float time;
			public float hp;
			public float energy;
			public Vector3 position;
			public float yVelocity;

			public Point (Player player) : this (player, player.timeline.currentTime)
			{
			}

			public Point (Player player, float time) : this (time, player.hp, player.energy, player.trs.position, player.yVelocity)
			{
			}

			public Point (float time, float hp, float energy, Vector3 position, float yVelocity)
			{
				this.time = time;
				this.hp = hp;
				this.energy = energy;
				this.position = position;
				this.yVelocity = yVelocity;
			}

			public void ApplyToPlayer (Player player)
			{
				player.hp = hp;
				player.trs.position = position;
				player.yVelocity = yVelocity;
				player.energy = energy;
				player.timeline.currentTime = time;
				if (time == 0 || time == player.timeline.timeOfLastPoint)
					player.timeline.enabled = false;
				else
					player.timeline.enabled = true;
			}
		}
	}
}