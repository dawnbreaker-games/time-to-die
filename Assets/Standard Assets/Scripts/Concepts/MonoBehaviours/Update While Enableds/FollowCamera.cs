using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace TimeToDie
{
	[ExecuteInEditMode]
	public class FollowCamera : UpdateWhileEnabled
	{
		public Transform trs;
		public FloatRange distanceRange;
		[HideInInspector]
		public FloatRange distanceRangeSqr;
		public float maxAngle;
		public float lerpRate;
		Vector3 targetPosition;
		Quaternion targetRotation;

#if UNITY_EDITOR
		void OnValidate ()
		{
			distanceRangeSqr = new FloatRange(distanceRange.min * distanceRange.min, distanceRange.max * distanceRange.max);
		}
#endif

		public override void DoUpdate ()
		{
			float distanceSqr = (trs.position - VRCameraRig.instance.eyesTrs.position).sqrMagnitude;
			if (Quaternion.Angle(trs.rotation, VRCameraRig.instance.eyesTrs.rotation) > maxAngle || !distanceRangeSqr.Contains(distanceSqr, true))
			{
				targetPosition = VRCameraRig.instance.eyesTrs.position + VRCameraRig.instance.eyesTrs.forward * Mathf.Clamp(Mathf.Sqrt(distanceSqr), distanceRange.min, distanceRange.max);
				targetRotation = Quaternion.LookRotation(VRCameraRig.instance.eyesTrs.forward, VRCameraRig.instance.eyesTrs.up);
			}
			trs.position = Vector3.Lerp(trs.position, targetPosition, lerpRate * Time.deltaTime);
			trs.rotation = Quaternion.Lerp(trs.rotation, targetRotation, lerpRate * Time.deltaTime);
		}
	}
}