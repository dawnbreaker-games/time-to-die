using Extensions;
using UnityEngine;
using System.Collections.Generic;

namespace TimeToDie
{
	public class Enemy : Entity
	{
		public FloatRange targetRangeFromPlayer;
		public BulletPatternEntry[] bulletPatternEntries = new BulletPatternEntry[0];
		public Dictionary<string, BulletPatternEntry> bulletPatternEntriesDict = new Dictionary<string, BulletPatternEntry>();
		public static List<Enemy> instances = new List<Enemy>();

		public override void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			base.OnEnable ();
			instances.Add(this);
		}

		public override void OnDisable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			base.OnDisable ();
			instances.Remove(this);
		}

		public override void Awake ()
		{
			base.Awake ();
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			for (int i = 0; i < bulletPatternEntries.Length; i ++)
			{
				BulletPatternEntry bulletPatternEntry = bulletPatternEntries[i];
				bulletPatternEntriesDict.Add(bulletPatternEntry.name, bulletPatternEntry);
			}
		}

		public void ShootBulletPatternEntry (string name)
		{
			bulletPatternEntriesDict[name].Shoot ();
		}
		
		public override void DoUpdate ()
		{
			if (GameManager.paused)
				return;
			HandleAttacking ();
			base.DoUpdate ();
		}

		public override void HandleRotating ()
		{
			trs.LookAt(VRCameraRig.instance.eyesTrs);
		}

		public override void HandleMoving ()
		{
			Vector3 toPlayer = Player.instance.trs.position - trs.position;
			float toPlayerDistanceSqr = toPlayer.sqrMagnitude;
			if (toPlayerDistanceSqr > targetRangeFromPlayer.max * targetRangeFromPlayer.max)
				rigid.velocity = toPlayer.normalized * moveSpeed;
			else if (toPlayerDistanceSqr < targetRangeFromPlayer.min * targetRangeFromPlayer.min)
				rigid.velocity = -toPlayer.normalized * moveSpeed;
		}

		public virtual void HandleAttacking ()
		{
		}
	}
}