﻿using System;
using Extensions;
using UnityEngine;
using System.Collections;
using UnityEngine.InputSystem;
using System.Collections.Generic;

namespace TimeToDie
{
	public class VRCameraRig : SingletonUpdateWhileEnabled<VRCameraRig>
	{
		public Camera camera;
		public Transform trackingSpaceTrs;
		public Transform eyesTrs;
		public Transform trs;
		public float rotateHands;
		public Hand leftHand;
		public Hand rightHand;
		public Transform bothHandsAverageTrs;
		[HideInInspector]
		public LayerMask whatIsVisible;
		public BoundingFrustum boundingFrustum = new BoundingFrustum();

		public override void OnEnable ()
		{
			base.OnEnable ();
			whatIsVisible = camera.cullingMask;
		}

		public override void DoUpdate ()
		{
			leftHand.previousTriggerInput = leftHand.triggerInput;
			leftHand.previousGripInput = leftHand.gripInput;
			leftHand.previousThumbstickClickedInput = leftHand.thumbstickClickedInput;
			rightHand.previousThumbstickClickedInput = rightHand.thumbstickClickedInput;
			rightHand.previousTriggerInput = rightHand.triggerInput;
			rightHand.previousGripInput = rightHand.gripInput;
			leftHand.previousPrimaryButtonInput = leftHand.primaryButtonInput;
			rightHand.previousPrimaryButtonInput = rightHand.primaryButtonInput;
			leftHand.previousSecondaryButtonInput = leftHand.secondaryButtonInput;
			rightHand.previousSecondaryButtonInput = rightHand.secondaryButtonInput;
			leftHand.previousPosition = leftHand.trs.position;
			rightHand.previousPosition = rightHand.trs.position;
			leftHand.previousThumbstickInput = leftHand.thumbstickInput;
			rightHand.previousThumbstickInput = rightHand.thumbstickInput;
			leftHand.triggerInput = InputManager.LeftTriggerInput;
			leftHand.gripInput = InputManager.LeftGripInput;
			rightHand.triggerInput = InputManager.RightTriggerInput;
			rightHand.gripInput = InputManager.RightGripInput;
			leftHand.thumbstickClickedInput = InputManager.LeftThumbstickClickedInput;
			rightHand.thumbstickClickedInput = InputManager.RightThumbstickClickedInput;
			if (InputManager.LeftThumbstickInput != null)
				leftHand.thumbstickInput = (Vector2) InputManager.LeftThumbstickInput;
			if (InputManager.RightThumbstickInput != null)
				rightHand.thumbstickInput = (Vector2) InputManager.RightThumbstickInput;
			leftHand.primaryButtonInput = InputManager.LeftPrimaryButtonInput;
			rightHand.primaryButtonInput = InputManager.RightPrimaryButtonInput;
			leftHand.secondaryButtonInput = InputManager.LeftSecondaryButtonInput;
			rightHand.secondaryButtonInput = InputManager.RightSecondaryButtonInput;
			if (InputManager.Instance.inputDevice == InputManager.InputDevice.VR)
				UpdateTransforms ();
			boundingFrustum.Update (camera, eyesTrs, camera.farClipPlane);
		}

		void UpdateTransforms ()
		{
			if (InputManager.HeadPosition != null)
				eyesTrs.localPosition = (Vector3) InputManager.HeadPosition;
			if (InputManager.HeadRotation != null)
				eyesTrs.localRotation = (Quaternion) InputManager.HeadRotation;
			if (InputManager.LeftHandPosition != null)
				leftHand.trs.localPosition = (Vector3) InputManager.LeftHandPosition;
			if (InputManager.LeftHandRotation != null)
			{
				leftHand.trs.localRotation = (Quaternion) InputManager.LeftHandRotation;
				leftHand.trs.Rotate(Vector3.right * rotateHands);
			}
			if (InputManager.RightHandPosition != null)
				rightHand.trs.localPosition = (Vector3) InputManager.RightHandPosition;
			if (InputManager.RightHandRotation != null)
			{
				rightHand.trs.localRotation = (Quaternion) InputManager.RightHandRotation;
				rightHand.trs.Rotate(Vector3.right * rotateHands);
			}
			bothHandsAverageTrs.position = (leftHand.trs.position + rightHand.trs.position) / 2;
			bothHandsAverageTrs.rotation = Quaternion.Slerp(leftHand.trs.rotation, rightHand.trs.rotation, 0.5f);
			bothHandsAverageTrs.SetWorldScale (Vector3.one * Vector3.Distance(leftHand.trs.position, rightHand.trs.position));

		}
		
		[Serializable]
		public class Hand
		{
			public bool isLeftHand;
			public Transform trs;
			public bool triggerInput;
			public bool previousTriggerInput;
			public bool gripInput;
			public bool previousGripInput;
			public bool primaryButtonInput;
			public bool previousPrimaryButtonInput;
			public bool secondaryButtonInput;
			public bool previousSecondaryButtonInput;
			public bool thumbstickClickedInput;
			public bool previousThumbstickClickedInput;
			public Vector2 thumbstickInput;
			public Vector2 previousThumbstickInput;
			public Vector3 previousPosition;
		}
	}
}