using TMPro;
using System;
using Extensions;
using UnityEngine;
using Random = UnityEngine.Random;

namespace TimeToDie
{
	public class ProceduralEndlessLevel : EndlessLevel, IUpdatable
	{
		public EnemySpawnEntry[] enemySpawnEntries = new EnemySpawnEntry[0];
		public FollowWaypoints waypointFollowerPrefab;
		public _Text scoreText;
		public _Text bestScoreReachedText;
		public float score;
		public float scorePerSecond;
		public float scorePerDamage;
		public float BestScoreReached
		{
			get
			{
				return PlayerPrefs.GetFloat(name + " best score", 0);
			}
			set
			{
				PlayerPrefs.SetFloat(name + " best score", value);
			}
		}

		public override void Awake ()
		{
			base.Awake ();
			bestScoreReachedText.Text = "Best score: " + string.Format("{0:0}", BestScoreReached);
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public void DoUpdate ()
		{
			score += Time.deltaTime * scorePerSecond;
			scoreText.Text = "Score: " + string.Format("{0:0}", score);
			for (int i = 0; i < enemySpawnEntries.Length; i ++)
			{
				EnemySpawnEntry enemySpawnEntry = enemySpawnEntries[i];
				int spawnCount = (int) enemySpawnEntry.spawnsOverTime.Evaluate(Time.timeSinceLevelLoad * enemySpawnEntry.timeMultiplier) - enemySpawnEntry.spawnedCount;
				for (int i2 = 0; i2 < spawnCount; i2 ++)
					SpawnEnemy (enemySpawnEntry);
				enemySpawnEntries[i].spawnedCount += spawnCount;
			}
		}

		void OnDestroy ()
		{
			GameManager.updatables = GameManager.updatables.Remove(this);
			if (score > BestScoreReached)
				BestScoreReached = score;
		}

		Enemy SpawnEnemy (EnemySpawnEntry enemySpawnEntry)
		{
			Vector3 spawnPosition;
			SpawnZone spawnZone;
			do
			{
				spawnZone = enemySpawnEntry.spawnZones[Random.Range(0, enemySpawnEntry.spawnZones.Length)];
				spawnPosition = spawnZone.GetRect3D().GetRandomPoint();
			} while ((Player.instance.trs.position - spawnPosition).sqrMagnitude < enemySpawnEntry.minSpawnDistanceToPlayer * enemySpawnEntry.minSpawnDistanceToPlayer);
			Enemy enemy = Instantiate(enemySpawnEntry.prefab, spawnPosition, enemySpawnEntry.prefab.trs.rotation);
			FollowWaypoints waypointFollower = ObjectPool.instance.SpawnComponent<FollowWaypoints>(waypointFollowerPrefab, spawnPosition);
			spawnZone = enemySpawnEntry.spawnZones[Random.Range(0, enemySpawnEntry.spawnZones.Length)];
			Vector3 waypointPosition = spawnZone.GetRect3D().GetRandomPoint();
			waypointFollower.waypoints[0].trs.position = waypointPosition;
			spawnZone = enemySpawnEntry.spawnZones[Random.Range(0, enemySpawnEntry.spawnZones.Length)];
			waypointPosition = spawnZone.GetRect3D().GetRandomPoint();
			waypointFollower.waypoints[1].trs.position = waypointPosition;
			spawnZone = enemySpawnEntry.spawnZones[Random.Range(0, enemySpawnEntry.spawnZones.Length)];
			waypointPosition = spawnZone.GetRect3D().GetRandomPoint();
			waypointFollower.waypoints[2].trs.position = waypointPosition;
			waypointFollower.moveSpeed = enemy.moveSpeed;
			waypointFollower.trs.localScale = Vector3.one;
			waypointFollower.enabled = true;
			enemy.moveSpeed = 0;
			enemy.trs.localScale = Vector3.one;
			enemy.onTakeDamage += OnEnemyTakeDamage;
			return enemy;
		}

		void OnEnemyTakeDamage (float amount)
		{
			ProceduralEndlessLevel proceduralEndlessLevel = Level.instance as ProceduralEndlessLevel;
			if (proceduralEndlessLevel != null)
				proceduralEndlessLevel.score += amount * scorePerDamage;
		}

		[Serializable]
		public struct SpawnZone
		{
			public Transform trs;
			public BoxCollider boxCollider;

			public Rect3D GetRect3D ()
			{
				return new Rect3D(boxCollider.bounds.center, boxCollider.size.Multiply(trs.lossyScale), trs.eulerAngles);
			}
		}

		[Serializable]
		public class SpawnEntry
		{
			public SpawnZone[] spawnZones;
			public int spawnedCount;
			public AnimationCurve spawnsOverTime;
			public float timeMultiplier;
		}

		[Serializable]
		public class SpawnEntry<T> : SpawnEntry
		{
			public T prefab;
		}

		[Serializable]
		public class EnemySpawnEntry : SpawnEntry<Enemy>
		{
			public float minSpawnDistanceToPlayer;
		}
	}
}