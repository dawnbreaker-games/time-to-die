using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace TimeToDie
{
	public class Item : MonoBehaviour
	{
		public uint cost;
		public Transform trs;
		public Transform uiTrs;
		public _Text costText;
		public GameObject graphicsAndColliderrGo;
		[HideInInspector]
		public float uiTrsHeight;
		[Multiline]
		public string description;

		public virtual void Awake ()
		{
			costText.Text = "Cost: " + cost;
			uiTrsHeight = uiTrs.localPosition.y;
		}

		public virtual void OnGain ()
		{
		}
	}
}