﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace TimeToDie
{
	public class Hazard : Spawnable
	{
		public float damage;
		
		public virtual void OnCollisionEnter (Collision coll)
		{
			IDestructable destructable = coll.collider.GetComponentInParent<IDestructable>();
			if (destructable != null)
				destructable.TakeDamage (damage);
		}
	}
}