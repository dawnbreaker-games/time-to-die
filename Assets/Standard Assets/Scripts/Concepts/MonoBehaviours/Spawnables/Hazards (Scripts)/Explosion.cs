using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace TimeToDie
{
	public class Explosion : Hazard
	{
		public uint maxHits;
		public LayerMask whatIDamage;
		public SphereCollider sphereCollider;
		
		public virtual void OnTriggerEnter (Collider collider)
		{
			IDestructable destructable = collider.GetComponentInParent<IDestructable>();
			destructable.TakeDamage (damage);
		}

		public void DamageDestructables ()
		{
			Collider[] hitColliders = Physics.OverlapSphere(trs.position, sphereCollider.bounds.extents.x, whatIDamage);
			List<Transform> hitTransforms = new List<Transform>();
			for (int i = 0; i < hitColliders.Length; i ++)
			{
				Collider hitCollider = hitColliders[i];
				hitTransforms.Add(hitCollider.GetComponent<Transform>());
			}
			for (int i = 0; i < Mathf.Min(hitColliders.Length, maxHits); i ++)
			{
				Transform closestTrs = trs.GetClosestTransform_3D(hitTransforms.ToArray());
				hitTransforms.Remove(closestTrs);
				IDestructable destructable = closestTrs.GetComponent<IDestructable>();
				destructable.TakeDamage (damage);
			}
		}
	}
}