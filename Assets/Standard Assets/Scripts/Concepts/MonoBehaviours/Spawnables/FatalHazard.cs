﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace TimeToDie
{
	public class FatalHazard : Spawnable
	{
		public virtual void OnCollisionEnter (Collision coll)
		{
			IDestructable destructable = coll.collider.GetComponentInParent<IDestructable>();
			if (destructable != null)
				destructable.Death ();
		}
	}
}