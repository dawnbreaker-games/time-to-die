using Extensions;
using UnityEngine;

namespace TimeToDie
{
	public class MakeMagicIndicator : MonoBehaviour
	{
		public MagicIndicator magicIndicator;

		void Awake ()
		{
			magicIndicator.trs.SetParent(MagicLocater.Instance.trs);
			magicIndicator.trs.localScale = magicIndicator.trs.lossyScale / MagicLocater.instance.range;
		}

		void OnEnable ()
		{
			magicIndicator.gameObject.SetActive(true);
			GameManager.updatables = GameManager.updatables.Add(magicIndicator);
		}

		void OnDisable ()
		{
#if UNITY_EDITOR
			if (magicIndicator != null)
#endif
			magicIndicator.gameObject.SetActive(false);
			GameManager.updatables = GameManager.updatables.Remove(magicIndicator);
		}

		void OnDestroy ()
		{
#if UNITY_EDITOR
			if (magicIndicator != null)
#endif
				Destroy(magicIndicator.gameObject);
		}
	}
}