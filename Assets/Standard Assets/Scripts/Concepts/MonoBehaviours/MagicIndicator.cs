using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace TimeToDie
{
	public class MagicIndicator : MonoBehaviour, IUpdatable
	{
		public Transform trs;
		public Transform trackedTrs;
		
		public void DoUpdate ()
		{
			Quaternion cameraRotation = VRCameraRig.instance.eyesTrs.rotation;
			trs.localPosition = Quaternion.Inverse(cameraRotation) * ((trackedTrs.position - Player.instance.trs.position) / MagicLocater.instance.range / 2);
			trs.localRotation = Quaternion.Inverse(cameraRotation) * trackedTrs.rotation;
			gameObject.SetActive(trs.localPosition.sqrMagnitude <= .25f && !VRCameraRig.instance.boundingFrustum.Contains(trackedTrs.position));
		}
	}
}