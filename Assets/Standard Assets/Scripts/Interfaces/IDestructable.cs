﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace TimeToDie
{
	public interface IDestructable
	{
		float Hp { get; set; }
		int MaxHp { get; set; }
		
		void TakeDamage (float amount);
		void Death ();
	}
}