using UnityEngine;
using System.Collections;

namespace TimeToDie
{
	public class BombBullet : Bullet
	{
		public bool useExplodeDelayAnimEntry;
		public AnimationEntry explodeDelayAnimEntry;
		public Explosion explosionPrefab;

		void OnTriggerEnter (Collider other)
		{
			if (other.GetComponentInParent<IDestructable>() != null)
			{
				if (useExplodeDelayAnimEntry)
					explodeDelayAnimEntry.Play ();
				else
					ObjectPool.instance.Despawn (prefabIndex, gameObject, trs);
			}
		}

		public override void OnDisable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			base.OnDisable ();
			Explosion explosion = ObjectPool.instance.SpawnComponent<Explosion>(explosionPrefab.prefabIndex, trs.position, Random.rotation);
			explosion.maxHits = hitsTillDespawn;
		}
	}
}