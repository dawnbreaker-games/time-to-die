using TMPro;
using Extensions;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Animations;
using UnityEngine.InputSystem;
using System.Collections.Generic;

namespace TimeToDie
{
	public class Player : SingletonUpdateWhileEnabled<Player>, IDestructable
	{
		[HideInInspector]
		public float hp;
		public float Hp
		{
			get
			{
				return hp;
			}
			set
			{
				hp = value;
			}
		}
		public int maxHp;
		public int MaxHp
		{
			get
			{
				return maxHp;
			}
			set
			{
				maxHp = value;
			}
		}
		public Transform trs;
		public CharacterController characterController;
		public float rotateRate;
		public float moveSpeed;
		public float jumpSpeed;
		public _Text hpText;
		public _Text energyText;
		public float minThumbstickInputMagnitudeToTurn;
		public InputActionReference rotateInput;
		public BulletPatternEntry bulletPatternEntry;
		public float bulletPatternFireRate;
		public Transform groundCheckSphereTrs;
		public LayerMask whatIsGround;
		public Timeline timeline;
		public float energy;
		public float timeLeapCost;
		public float timeDuplicateCost;
		[HideInInspector]
		public float yVelocity;
		List<DuplicateTimelinePoint> duplicateTimelinePoints = new List<DuplicateTimelinePoint>();
		float bulletPatternFireTimer;
		bool dead;
		bool previousJumpInput;
		bool isGrounded;
		bool rewinding;
		bool previousRewindInput;

		public override void Awake ()
		{
			base.Awake ();
			hp = maxHp;
			hpText.Text = "Hp: " + (uint) hp + "/" + maxHp;
			rotateInput.action.Enable();
		}

		public override void DoUpdate ()
		{
			characterController.Move(Quaternion.LookRotation(VRCameraRig.instance.trs.forward.SetY(0), Vector3.up) * (InputManager.MovementInput * moveSpeed * Time.deltaTime).SetY(yVelocity));
			Vector2 rotate = rotateInput.action.ReadValue<Vector2>() * rotateRate * Time.deltaTime;
			Vector3 newFacing = Quaternion.Euler(Vector3.up * rotate.x + VRCameraRig.instance.trs.right * -rotate.y) * VRCameraRig.instance.trs.forward;
			Quaternion newRotation = Quaternion.LookRotation(newFacing, Vector3.up);
			if (Quaternion.Angle(VRCameraRig.instance.trs.rotation, newRotation) > 160 && rotate.magnitude < 160)
			{
			}
			else
				VRCameraRig.instance.trs.rotation = newRotation;
			SetIsGrounded ();
			if (!isGrounded)
				yVelocity += Physics.gravity.y * Time.deltaTime;
			else
				yVelocity = 0;
			HandleTimeline ();
			HandleShooting ();
			HandleJumping ();
		}

		public void TakeDamage (float amount)
		{
			if (dead)
				return;
			hp = Mathf.Clamp(hp - amount, 0, MaxHp);
			hpText.Text = "Hp: " + (uint) hp + "/" + maxHp;
			if (hp == 0)
			{
				dead = true;
				Death ();
			}
		}

		void HandleShooting ()
		{
			if (bulletPatternFireTimer >= bulletPatternFireRate)
			{
				if (InputManager.ShootInput)
				{
					Bullet[] bullets = bulletPatternEntry.Shoot();
					for (int i = 0; i < bullets.Length; i ++)
					{
						Bullet bullet = bullets[i];
						bullet.shooter = characterController;
						Physics.IgnoreCollision(bullet.collider, characterController, true);
						bullet.collider.enabled = true;
					}
					bulletPatternFireTimer -= bulletPatternFireRate;
				}
			}
			else
				bulletPatternFireTimer += Time.deltaTime;
		}

		void HandleJumping ()
		{
			bool jumpInput = InputManager.JumpInput;
			if (jumpInput && !previousJumpInput)
			{
				if (isGrounded)
					yVelocity = jumpSpeed;
			}
			else if (!jumpInput && previousJumpInput && yVelocity > 0)
				yVelocity = 0;
			previousJumpInput = jumpInput;
		}

		void HandleTimeline ()
		{
			bool rewindInput = InputManager.RewindInput;
			if (rewindInput && !previousRewindInput)
				rewinding = true;
			else if (!rewindInput && previousRewindInput)
				rewinding = false;
			float previousTime = timeline.currentTime;
			float deltaTime = Time.deltaTime;
			if (!rewinding)
				timeline.currentTime += deltaTime;
			else
				timeline.currentTime -= deltaTime;
			energy += deltaTime;
			timeline.InsertPointForPlayerAtTime (previousTime, timeline.currentTime, this);
			Vector2 normalizedPoint = timeline.backgroundImage.rectTransform.rect.ToNormalizedPoint((Vector2) InputManager.MousePosition);
			normalizedPoint.x -= .5f;
			if (normalizedPoint.x >= 0 && normalizedPoint.x <= 1 && normalizedPoint.y >= 0 && normalizedPoint.y <= 1)
			{
				if (Mouse.current.leftButton.wasPressedThisFrame)
				{
					if (energy >= timeLeapCost)
					{
						timeline.ApplyToPlayerAtTime (timeline.timeOfLastPoint * normalizedPoint.x, this);
						energy -= timeLeapCost;
					}
				}
				if (Mouse.current.rightButton.wasPressedThisFrame)
				{
					if (energy >= timeDuplicateCost)
					{
						Transform timeIndicatorTrs = Instantiate(timeline.currentTimeIndicatorTrs.parent, timeline.currentTimeIndicatorTrs.parent.parent).GetChild(0);
						timeIndicatorTrs.GetComponent<Image>().color = Color.black;
						duplicateTimelinePoints.Add(new DuplicateTimelinePoint(timeIndicatorTrs, timeline.timeOfLastPoint * normalizedPoint.x));
						energy -= timeDuplicateCost;
					}
				}
			}
			for (int i = 0; i < duplicateTimelinePoints.Count; i ++)
			{
				DuplicateTimelinePoint duplicateTimelinePoint = duplicateTimelinePoints[i];
				if (!rewinding)
					duplicateTimelinePoint.duplicateToTime += deltaTime;
				else
					duplicateTimelinePoint.duplicateToTime -= deltaTime;
				duplicateTimelinePoint.timeIndicatorTrs.localScale = duplicateTimelinePoint.timeIndicatorTrs.localScale.SetX(timeline.timeOfLastPoint / duplicateTimelinePoint.duplicateToTime);
				duplicateTimelinePoint.timeIndicatorTrs.parent.localScale = duplicateTimelinePoint.timeIndicatorTrs.parent.localScale.SetX(duplicateTimelinePoint.duplicateToTime / timeline.timeOfLastPoint);
				timeline.InsertPointForPlayerAtTime (duplicateTimelinePoint.duplicateToTime + (previousTime - timeline.currentTime), duplicateTimelinePoint.duplicateToTime, this);
				duplicateTimelinePoints[i] = duplicateTimelinePoint;
			}
			energyText.Text = "Energy: " + energy.ToString("F1");
			previousRewindInput = rewindInput;
		}

		void SetIsGrounded ()
		{
			isGrounded = Physics.CheckSphere(groundCheckSphereTrs.position, groundCheckSphereTrs.lossyScale.x / 2, whatIsGround);
		}

		public void Death ()
		{
			GameOverMenu.Instance.Open ();
		}

		struct DuplicateTimelinePoint
		{
			public Transform timeIndicatorTrs;
			public float duplicateToTime;

			public DuplicateTimelinePoint (Transform timeIndicatorTrs, float duplicateToTime)
			{
				this.timeIndicatorTrs = timeIndicatorTrs;
				this.duplicateToTime = duplicateToTime;
			}
		}
	}
}