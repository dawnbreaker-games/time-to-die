﻿using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace TimeToDie
{
	public class Bullet : Hazard
	{
		public float range;
		public float duration;
		public Rigidbody rigid;
		public float moveSpeed;
		public AutoDespawnMode autoDespawnMode;
		public ObjectPool.RangedDespawn rangedDespawn;
		public ObjectPool.DelayedDespawn delayedDespawn;
		public new Collider collider;
		public uint hitsTillDespawn;
		public LayerMask whatReducesHits;
		[HideInInspector]
		public bool dead;
		[HideInInspector]
		public Collider shooter;
		public static List<Bullet> instances = new List<Bullet>();
		uint hitsTillDespawnRemaining;
		
		public virtual void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				if (rigid == null)
					rigid = GetComponent<Rigidbody>();
				if (collider == null)
					collider = GetComponentInChildren<Collider>();
				return;
			}
#endif
			dead = false;
			hitsTillDespawnRemaining = hitsTillDespawn;
			if (autoDespawnMode == AutoDespawnMode.RangedAutoDespawn)
				rangedDespawn = ObjectPool.instance.RangeDespawn(prefabIndex, gameObject, trs, range);
			else if (autoDespawnMode == AutoDespawnMode.DelayedAutoDespawn)
				delayedDespawn = ObjectPool.instance.DelayDespawn(prefabIndex, gameObject, trs, duration);
			rigid.velocity = trs.forward * moveSpeed;
			instances.Add(this);
		}

		public virtual void OnDisable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			StopAllCoroutines();
			if (ObjectPool.instance != null)
			{
				if (autoDespawnMode == AutoDespawnMode.RangedAutoDespawn)
					ObjectPool.instance.CancelRangedDespawn (rangedDespawn);
				else if (autoDespawnMode == AutoDespawnMode.DelayedAutoDespawn)
					ObjectPool.instance.CancelDelayedDespawn (delayedDespawn);
			}
			if (shooter != null)
				Physics.IgnoreCollision(collider, shooter, false);
			collider.enabled = false;
			dead = true;
			instances.Remove(this);
		}

		public override void OnCollisionEnter (Collision coll)
		{
			if (!dead)
			{
				base.OnCollisionEnter (coll);
				if (whatReducesHits.ContainsLayer(coll.gameObject.layer))
				{
					hitsTillDespawnRemaining --;
					if (hitsTillDespawnRemaining == 0)
						ObjectPool.instance.Despawn (prefabIndex, gameObject, trs);
				}
			}
		}

		public enum AutoDespawnMode
		{
			DontAutoDespawn,
			RangedAutoDespawn,
			DelayedAutoDespawn
		}
	}
}