﻿using TMPro;
using System;
using Extensions;
using UnityEngine;
using System.Collections;
using UnityEngine.InputSystem;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace TimeToDie
{
	public class GameManager : SingletonMonoBehaviour<GameManager>, ISaveableAndLoadable
	{
		public _Text notificationText;
		public SerializableDictionary<MenuType, Menu> menusDict = new SerializableDictionary<MenuType, Menu>();
		public float switchMenuRate;
		public static Menu currentMenu;
		public static bool paused;
		public static IUpdatable[] updatables = new IUpdatable[0];
		public static int framesSinceLevelLoaded;
		public static bool isQuitting;
		public static float pausedTime;
		public static float TimeSinceLevelLoad
		{
			get
			{
				return Time.timeSinceLevelLoad - pausedTime;
			}
		}
		MenuType currentMenuType;
		float switchMenuTimer;
		int previousSwitchMenuInput;
		bool previousMenuInput;

		public override void Awake ()
		{
			base.Awake ();
			if (instance != this)
				return;
			menusDict.Init ();
			Cursor.lockState = CursorLockMode.Locked;
			framesSinceLevelLoaded = 0;
			pausedTime = 0;
		}

		void Update ()
		{
			for (int i = 0; i < updatables.Length; i ++)
			{
				IUpdatable updatable = updatables[i];
				updatable.DoUpdate ();
			}
			if (!paused && Time.deltaTime > 0)
			{
				Physics.Simulate(Time.deltaTime);
				if (ObjectPool.instance.enabled)
					ObjectPool.instance.DoUpdate ();
				pausedTime += Time.unscaledDeltaTime;
			}
			InputSystem.Update ();
			if (Keyboard.current.leftAltKey.isPressed)
				Cursor.lockState = CursorLockMode.None;
			else
				Cursor.lockState = CursorLockMode.Locked;
			// if (!GameOverMenu.Instance.gameObject.activeSelf)
			// {
			// 	bool menuInput = InputManager.MenuInput;
			// 	int switchMenuInput = InputManager.SwitchMenuInput;
			// 	if (menuInput && !previousMenuInput)
			// 	{
			// 		if (currentMenu.gameObject.activeSelf)
			// 			currentMenu.Close ();
			// 		else
			// 			currentMenu.Open ();
			// 	}
			// 	if (switchMenuInput != 0 && currentMenu.gameObject.activeSelf)
			// 	{
			// 		switchMenuTimer -= Time.deltaTime;
			// 		if (Mathf.Sign(switchMenuInput) != MathfExtensions.Sign(previousSwitchMenuInput) || switchMenuTimer < 0)
			// 		{
			// 			switchMenuTimer = switchMenuRate;
			// 			int currentMenuType = this.currentMenuType.GetHashCode() + switchMenuInput;
			// 			int menuTypeCount = Enum.GetNames(typeof(MenuType)).Length;
			// 			if (currentMenuType == menuTypeCount)
			// 				currentMenuType = 0;
			// 			else if (currentMenuType == -1)
			// 				currentMenuType = menuTypeCount - 1;
			// 			this.currentMenuType = (MenuType) Enum.ToObject(typeof(MenuType), currentMenuType);
			// 			SetCurrentMenu (this.currentMenuType);
			// 		}
			// 	}
			// 	else
			// 		switchMenuTimer = switchMenuRate;
			// 	previousMenuInput = menuInput;
			// 	previousSwitchMenuInput = switchMenuInput;
			// }
			framesSinceLevelLoaded ++;
		}

		public void SetCurrentMenu (MenuType menuType)
		{
			currentMenuType = menuType;
			currentMenu.Close ();
			currentMenu = menusDict[currentMenuType];
			currentMenu.Open ();
		}

		public void Quit ()
		{
			Application.Quit();
		}

		void OnApplicationQuit ()
		{
			isQuitting = true;
			// PlayerPrefs.DeleteAll();
			// SaveAndLoadManager.instance.Save ("Auto-Save");
		}

		public void ToggleGameObject (GameObject go)
		{
			go.SetActive(!go.activeSelf);
		}

		public void DestroyChildren (Transform trs)
		{
			for (int i = 0; i < trs.childCount; i ++)
				Destroy(trs.GetChild(i).gameObject);
		}

		public void DestroyChildrenImmediate (Transform trs)
		{
			for (int i = 0; i < trs.childCount; i ++)
				DestroyImmediate(trs.GetChild(i).gameObject);
		}

		public static void Log (object obj)
		{
			print(obj);
		}
		
#if UNITY_EDITOR
		public static void DestroyOnNextEditorUpdate (Object obj)
		{
			EditorApplication.update += () => { if (obj == null) return; DestroyObject (obj); };
		}

		static void DestroyObject (Object obj)
		{
			if (obj == null)
				return;
			EditorApplication.update -= () => { DestroyObject (obj); };
			DestroyImmediate(obj);
		}
#endif

		public static void DoActionToObjectAndSpawnedInstances<T> (Action<T> action, T obj) where T : ISpawnable
		{
			action (obj);
			for (int i = 0; i < ObjectPool.instance.spawnedEntries.Count; i ++)
			{
				ObjectPool.SpawnedEntry spawnedEntry = ObjectPool.instance.spawnedEntries[i];
				T instance = spawnedEntry.go.GetComponent<T>();
				if (obj != null)
					action(obj);
			}
		}

		[Serializable]
		public class ItemEntry
		{
			public Item itemPrefab;
			public uint maxCount;
		}

		public enum MenuType
		{
			WorldMap,
			Inventory,
			Settings
		}
	}
}