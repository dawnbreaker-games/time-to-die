﻿using System;
using System.IO;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

[ExecuteInEditMode]
public class SaveAndLoadManager : SingletonMonoBehaviour<SaveAndLoadManager>
{
	public static SaveData saveData;
	public static string MostRecentSaveFileName
	{
		get
		{
			return PlayerPrefs.GetString("Most recent save file name", null);
		}
		set
		{
			PlayerPrefs.SetString("Most recent save file name", value);
		}
	}
	
	public void Start ()
	{
#if UNITY_EDITOR
		if (!Application.isPlaying)
			return;
#endif
		if (!string.IsNullOrEmpty(MostRecentSaveFileName))
			LoadMostRecent ();
	}

	void OnAboutToSave ()
	{
	}
	
	public void Save (string fileName)
	{
		OnAboutToSave ();
		FileStream fileStream = new FileStream(fileName, FileMode.Create);
		BinaryFormatter binaryFormatter = new BinaryFormatter();
		binaryFormatter.Serialize(fileStream, saveData);
		fileStream.Close();
		MostRecentSaveFileName = fileName;
	}
	
	public void Load (string fileName)
	{
		FileStream fileStream = new FileStream(fileName, FileMode.Open);
		BinaryFormatter binaryFormatter = new BinaryFormatter();
		saveData = (SaveData) binaryFormatter.Deserialize(fileStream);
		fileStream.Close();
		OnLoad (fileName);
	}

	void OnLoad (string fileName)
	{
		MostRecentSaveFileName = fileName;
		OnLoaded ();
	}

	void OnLoaded ()
	{
	}
	
	public void LoadMostRecent ()
	{
		Load (MostRecentSaveFileName);
	}
	
	[Serializable]
	public struct SaveData
	{
		public bool showTutorials;
	}
}