﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using System.IO;
using Extensions;
using UnityEditor;
using UnityEngine.UI;
using UnityEditor.SceneManagement;
using UnityEditor.Build.Reporting;
#endif

namespace TimeToDie
{
	//[ExecuteAlways]
	public class BuildManager : SingletonMonoBehaviour<BuildManager>
	{
#if UNITY_EDITOR
		public BuildAction[] buildActions;
		static BuildPlayerOptions buildOptions;
		public Text versionNumberText;
#endif
		public int versionIndex;
		public string versionNumberPrefix;
		public bool clearDataOnFirstStartup;
		public static bool IsFirstStartup
		{
			get
			{
				return PlayerPrefs.GetInt("1st startup", 1) == 1;
			}
			set
			{
				PlayerPrefs.SetInt("1st startup", value.GetHashCode());
			}
		}
		
#if UNITY_EDITOR
		public static string[] GetScenePathsInBuild ()
		{
			List<string> scenePathsInBuild = new List<string>();
			for (int i = 0; i < EditorBuildSettings.scenes.Length; i ++)
			{
				EditorBuildSettingsScene scene = EditorBuildSettings.scenes[i];
				if (scene.enabled)
					scenePathsInBuild.Add(scene.path);
			}
			return scenePathsInBuild.ToArray();
		}

		public static string[] GetAllScenePaths ()
		{
			List<string> scenePaths = new List<string>();
			for (int i = 0; i < EditorBuildSettings.scenes.Length; i ++)
				scenePaths.Add(EditorBuildSettings.scenes[i].path);
			return scenePaths.ToArray();
		}
		
		[MenuItem("Build/Make Builds %&b")]
		public static void Build ()
		{
			Instance._Build ();
		}

		public void _Build ()
		{
			EditorPrefs.SetInt("Current build action index", -1);
			Instance.versionIndex ++;
			for (int i = 0; i < buildActions.Length; i ++)
			{
				BuildAction buildAction = buildActions[i];
				if (buildAction.enabled)
				{
					EditorPrefs.SetInt("Current build action index", i);
					buildAction.Do ();
				}
			}
			EditorPrefs.SetInt("Current build action index", -1);
		}

		[UnityEditor.Callbacks.DidReloadScripts]
		public static void OnScriptsReload ()
		{
			int currentBuildActionIndex = EditorPrefs.GetInt("Current build action index", -1);
			if (currentBuildActionIndex != -1)
			{
				for (int i = currentBuildActionIndex; i < Instance.buildActions.Length; i ++)
				{
					BuildAction buildAction = instance.buildActions[i];
					if (buildAction.enabled)
					{
						EditorPrefs.SetInt("Current build action index", i);
						buildAction.Do ();
					}
				}
			}
			EditorPrefs.SetInt("Current build action index", -1);
		}
		
		[Serializable]
		public class BuildAction
		{
			public string name;
			public bool enabled;
			public BuildTarget target;
			public string locationPath;
			public BuildOptions[] options;
			public InputManager.InputDevice inputDevice;
			public bool removeExtraFolders;
			public bool makeZip;
			public string directoryToZip;
			public string zipLocationPath;
			public bool clearDataOnFirstStartup;
			
			public void Do ()
			{
				if (target == BuildTarget.StandaloneOSX && PlayerSettings.GetScriptingBackend(BuildTargetGroup.Standalone) != ScriptingImplementation.Mono2x)
				{
					PlayerSettings.SetScriptingBackend(BuildTargetGroup.Standalone, ScriptingImplementation.Mono2x);
					return;
				}
				else if (target != BuildTarget.StandaloneOSX && PlayerSettings.GetScriptingBackend(BuildTargetGroup.Standalone) != ScriptingImplementation.IL2CPP)
				{
					PlayerSettings.SetScriptingBackend(BuildTargetGroup.Standalone, ScriptingImplementation.IL2CPP);
					return;
				}
				Instance.clearDataOnFirstStartup = clearDataOnFirstStartup;
				if (instance.versionNumberText != null)
					instance.versionNumberText.text = BuildManager.Instance.versionNumberPrefix + DateTime.Now.Date.ToString("MMdd");
				SetInputDevice.Set (inputDevice);
				EditorSceneManager.MarkAllScenesDirty();
				EditorSceneManager.SaveOpenScenes();
				buildOptions = new BuildPlayerOptions();
				buildOptions.scenes = GetScenePathsInBuild();
				buildOptions.target = target;
				buildOptions.locationPathName = locationPath;
				foreach (BuildOptions option in options)
					buildOptions.options |= option;
				BuildPipeline.BuildPlayer(buildOptions);
				AssetDatabase.Refresh();
				if (removeExtraFolders)
				{
					int indexOfDirectorySeperator = locationPath.LastIndexOf("/");
					if (indexOfDirectorySeperator == -1)
						indexOfDirectorySeperator = locationPath.LastIndexOf("\\");
					string folderPathPrefix = locationPath.Remove(indexOfDirectorySeperator + 1) + Application.productName;
					Directory.Delete(folderPathPrefix + "_BackUpThisFolder_ButDontShipItWithYourGame", true);
					Directory.Delete(folderPathPrefix + "_BurstDebugInformation_DoNotShip", true);
				}
				if (makeZip)
				{
					File.Delete(zipLocationPath);
					SystemExtensions.CompressDirectory (directoryToZip, zipLocationPath);
				}
			}
		}
#endif
	}
}