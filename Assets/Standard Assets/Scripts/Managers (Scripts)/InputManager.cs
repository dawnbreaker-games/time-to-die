﻿using System;
using Extensions;
using UnityEngine;
using System.Collections;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.XR;
using System.Collections.Generic;
using UnityEngine.XR.OpenXR.Features.Interactions;

namespace TimeToDie
{
	public class InputManager : SingletonMonoBehaviour<InputManager>
	{
		public InputDevice inputDevice;
		public InputSettings settings;
		public float minPressMagnitude;
		public static bool UsingGamepad
		{
			get
			{
				return Gamepad.current != null;
			}
		}
		public static bool UsingMouse
		{
			get
			{
				return Mouse.current != null;
			}
		}
		public static bool UsingKeyboard
		{
			get
			{
				return Keyboard.current != null;
			}
		}
		public static bool LeftClickInput
		{
			get
			{
				return UsingMouse && Mouse.current.leftButton.isPressed;
			}
		}
		public static bool RightClickInput
		{
			get
			{
				return UsingMouse && Mouse.current.rightButton.isPressed;
			}
		}
		public static Vector2? MousePosition
		{
			get
			{
				if (UsingMouse)
					return Mouse.current.position.ReadValue();
				else
					return null;
			}
		}
		public static bool RewindInput
		{
			get
			{
				return (UsingKeyboard && Keyboard.current.enterKey.isPressed) || (UsingGamepad && Gamepad.current.leftShoulder.isPressed);
			}
		}
		public static bool SubmitInput
		{
			get
			{
				return (UsingKeyboard && Keyboard.current.enterKey.isPressed) || (UsingGamepad && Gamepad.current.aButton.isPressed);
			}
		}
		public static Vector3 MovementInput
		{
			get
			{
				int x = 0;
				if (Keyboard.current.dKey.isPressed)
					x ++;
				if (Keyboard.current.aKey.isPressed)
					x --;
				int z = 0;
				if (Keyboard.current.wKey.isPressed)
					z ++;
				if (Keyboard.current.sKey.isPressed)
					z --;
				return Vector3.ClampMagnitude(new Vector3(x, 0, z), 1);
			}
		}
		public static Vector2 UIMovementInput
		{
			get
			{
				if (UsingGamepad)
					return Vector2.ClampMagnitude(Gamepad.current.leftStick.ReadValue(), 1);
				else
				{
					int x = 0;
					if (Keyboard.current.dKey.isPressed)
						x ++;
					if (Keyboard.current.aKey.isPressed)
						x --;
					int y = 0;
					if (Keyboard.current.wKey.isPressed)
						y ++;
					if (Keyboard.current.sKey.isPressed)
						y --;
					return Vector2.ClampMagnitude(new Vector2(x, y), 1);
				}
			}
		}
		public static Vector2? LeftThumbstickInput
		{
			get
			{
				if (LeftOculusTouchController != null)
					return Vector2.ClampMagnitude(LeftOculusTouchController.thumbstick.ReadValue(), 1);
				else if (LeftValveIndexController != null)
					return Vector2.ClampMagnitude(LeftValveIndexController.thumbstick.ReadValue(), 1);
				else if (LeftHTCViveController != null)
					return Vector2.ClampMagnitude(LeftHTCViveController.trackpad.ReadValue(), 1);
				else
					return null;
			}
		}
		public static Vector2? RightThumbstickInput
		{
			get
			{
				if (RightOculusTouchController != null)
					return Vector2.ClampMagnitude(RightOculusTouchController.thumbstick.ReadValue(), 1);
				else if (RightValveIndexController != null)
					return Vector2.ClampMagnitude(RightValveIndexController.thumbstick.ReadValue(), 1);
				else if (RightHTCViveController != null)
					return Vector2.ClampMagnitude(RightHTCViveController.trackpad.ReadValue(), 1);
				else
					return null;
			}
		}
		public static bool ShootInput
		{
			get
			{
				return (UsingMouse && LeftClickInput) || (UsingGamepad && RightTriggerInput);
			}
		}
		public static bool JumpInput
		{
			get
			{
				return (UsingKeyboard && Keyboard.current.spaceKey.isPressed) || (UsingGamepad && Gamepad.current.aButton.isPressed);
			}
		}
		public static bool MenuInput
		{
			get
			{
				return (LeftOculusTouchController != null && LeftOculusTouchController.menu.isPressed) || 
					(LeftValveIndexController != null && LeftValveIndexController.system.isPressed) || 
					(LeftHTCViveController != null && LeftHTCViveController.select.isPressed) || 
					(UsingKeyboard && Keyboard.current.tabKey.isPressed);
			}
		}
		public static int SwitchMenuInput
		{
			get
			{
				if (instance.inputDevice == InputDevice.VR)
				{
					Vector2? leftThumbstickInput = LeftThumbstickInput;
					if (leftThumbstickInput != null)
					{
						float leftThumbstickInputX = ((Vector2) leftThumbstickInput).x;
						if (leftThumbstickInputX > instance.minPressMagnitude)
							return 1;
						else if (leftThumbstickInputX < -instance.minPressMagnitude)
							return -1;
					}
					Vector2? rightThumbstickInput = RightThumbstickInput;
					if (rightThumbstickInput != null)
					{
						float rightThumbstickInputX = ((Vector2) rightThumbstickInput).x;
						if (rightThumbstickInputX > instance.minPressMagnitude)
							return 1;
						else if (rightThumbstickInputX < -instance.minPressMagnitude)
							return -1;
					}
				}
				else
				{
					if (Keyboard.current.rightArrowKey.isPressed)
						return 1;
					else if (Keyboard.current.leftArrowKey.isPressed)
						return -1;
				}
				return 0;
			}
		}
		public static bool LeftGripInput
		{
			get
			{
				return (LeftOculusTouchController != null && LeftOculusTouchController.gripPressed.isPressed) || 
					(LeftValveIndexController != null && LeftValveIndexController.gripPressed.isPressed) || 
					(LeftHTCViveController != null && LeftHTCViveController.gripPressed.isPressed);
			}
		}
		public static bool RightGripInput
		{
			get
			{
				return (RightOculusTouchController != null && RightOculusTouchController.gripPressed.isPressed) || 
					(RightValveIndexController != null && RightValveIndexController.gripPressed.isPressed) || 
					(RightHTCViveController != null && RightHTCViveController.gripPressed.isPressed);
			}
		}
		public static bool LeftTriggerInput
		{
			get
			{
				return (LeftOculusTouchController != null && LeftOculusTouchController.triggerPressed.isPressed) || 
					(LeftValveIndexController != null && LeftValveIndexController.triggerPressed.isPressed) || 
					(LeftHTCViveController != null && LeftHTCViveController.triggerPressed.isPressed);
			}
		}
		public static bool RightTriggerInput
		{
			get
			{
				return (RightOculusTouchController != null && RightOculusTouchController.triggerPressed.isPressed) || 
					(RightValveIndexController != null && RightValveIndexController.triggerPressed.isPressed) || 
					(RightHTCViveController != null && RightHTCViveController.triggerPressed.isPressed);
			}
		}
		public static bool LeftPrimaryButtonInput
		{
			get
			{
				return (LeftOculusTouchController != null && LeftOculusTouchController.primaryButton.isPressed) || 
					(LeftValveIndexController != null && LeftValveIndexController.primaryButton.isPressed); 
			}
		}
		public static bool RightPrimaryButtonInput
		{
			get
			{
				return (RightOculusTouchController != null && RightOculusTouchController.primaryButton.isPressed) || 
					(RightValveIndexController != null && RightValveIndexController.primaryButton.isPressed);
			}
		}
		public static bool LeftSecondaryButtonInput
		{
			get
			{
				return (LeftOculusTouchController != null && LeftOculusTouchController.secondaryButton.isPressed) || 
					(LeftValveIndexController != null && LeftValveIndexController.secondaryButton.isPressed); 
			}
		}
		public static bool RightSecondaryButtonInput
		{
			get
			{
				return (RightOculusTouchController != null && RightOculusTouchController.secondaryButton.isPressed) || 
					(RightValveIndexController != null && RightValveIndexController.secondaryButton.isPressed);
			}
		}
		public static bool LeftThumbstickClickedInput
		{
			get
			{
				return (LeftOculusTouchController != null && LeftOculusTouchController.thumbstickClicked.isPressed) || 
					(LeftValveIndexController != null && LeftValveIndexController.thumbstickClicked.isPressed) || 
					(LeftHTCViveController != null && LeftHTCViveController.trackpadClicked.isPressed);
			}
		}
		public static bool RightThumbstickClickedInput
		{
			get
			{
				return (RightOculusTouchController != null && RightOculusTouchController.thumbstickClicked.isPressed) || 
					(RightValveIndexController != null && RightValveIndexController.thumbstickClicked.isPressed) || 
					(RightHTCViveController != null && RightHTCViveController.trackpadClicked.isPressed);
			}
		}
		public static Vector3? HeadPosition
		{
			get
			{
				if (Hmd != null)
					return Hmd.devicePosition.ReadValue();
				else
					return null;
			}
		}
		public static Quaternion? HeadRotation
		{
			get
			{
				if (Hmd != null)
					return Hmd.deviceRotation.ReadValue();
				else
					return null;
			}
		}
		public static Vector3? LeftHandPosition
		{
			get
			{
				if (LeftOculusTouchController != null)
					return LeftOculusTouchController.devicePosition.ReadValue();
				else if (LeftValveIndexController != null)
					return LeftValveIndexController.devicePosition.ReadValue();
				else if (LeftHTCViveController != null)
					return LeftHTCViveController.devicePosition.ReadValue();
				else
					return null;
			}
		}
		public static Quaternion? LeftHandRotation
		{
			get
			{
				if (LeftOculusTouchController != null)
					return LeftOculusTouchController.deviceRotation.ReadValue();
				else if (LeftValveIndexController != null)
					return LeftValveIndexController.deviceRotation.ReadValue();
				else if (LeftHTCViveController != null)
					return LeftHTCViveController.deviceRotation.ReadValue();
				else
					return null;
			}
		}
		public static Vector3? RightHandPosition
		{
			get
			{
				if (RightOculusTouchController != null)
					return RightOculusTouchController.devicePosition.ReadValue();
				else if (RightValveIndexController != null)
					return RightValveIndexController.devicePosition.ReadValue();
				else if (RightHTCViveController != null)
					return RightHTCViveController.devicePosition.ReadValue();
				else
					return null;
			}
		}
		public static Quaternion? RightHandRotation
		{
			get
			{
				if (RightOculusTouchController != null)
					return RightOculusTouchController.deviceRotation.ReadValue();
				else if (RightValveIndexController != null)
					return RightValveIndexController.deviceRotation.ReadValue();
				else if (RightHTCViveController != null)
					return RightHTCViveController.deviceRotation.ReadValue();
				else
					return null;
			}
		}
		public static XRHMD Hmd
		{
			get
			{
				return InputSystem.GetDevice<XRHMD>();
			}
		}
		public static OculusTouchControllerProfile.OculusTouchController LeftOculusTouchController
		{
			get
			{
				return OculusTouchControllerProfile.OculusTouchController.leftHand as OculusTouchControllerProfile.OculusTouchController;
			}
		}
		public static OculusTouchControllerProfile.OculusTouchController RightOculusTouchController
		{
			get
			{
				return OculusTouchControllerProfile.OculusTouchController.rightHand as OculusTouchControllerProfile.OculusTouchController;
			}
		}
		public static ValveIndexControllerProfile.ValveIndexController LeftValveIndexController
		{
			get
			{
				return ValveIndexControllerProfile.ValveIndexController.leftHand as ValveIndexControllerProfile.ValveIndexController;
			}
		}
		public static ValveIndexControllerProfile.ValveIndexController RightValveIndexController
		{
			get
			{
				return ValveIndexControllerProfile.ValveIndexController.rightHand as ValveIndexControllerProfile.ValveIndexController;
			}
		}
		public static HTCViveControllerProfile.ViveController LeftHTCViveController
		{
			get
			{
				return HTCViveControllerProfile.ViveController.leftHand as HTCViveControllerProfile.ViveController;
			}
		}
		public static HTCViveControllerProfile.ViveController RightHTCViveController
		{
			get
			{
				return HTCViveControllerProfile.ViveController.rightHand as HTCViveControllerProfile.ViveController;
			}
		}

		public static float GetAxis (InputControl<float> positiveButton, InputControl<float> negativeButton)
		{
			return positiveButton.ReadValue() - negativeButton.ReadValue();
		}

		public static Vector2 GetAxis2D (InputControl<float> positiveXButton, InputControl<float> negativeXButton, InputControl<float> positiveYButton, InputControl<float> negativeYButton)
		{
			Vector2 output = new Vector2();
			output.x = positiveXButton.ReadValue() - negativeXButton.ReadValue();
			output.y = positiveYButton.ReadValue() - negativeYButton.ReadValue();
			output = Vector2.ClampMagnitude(output, 1);
			return output;
		}
		
		public enum InputDevice
		{
			KeyboardAndMouse,
			VR
		}
	}
}